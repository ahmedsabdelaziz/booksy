import {NextRequest, NextResponse} from 'next/server';

const PUBLIC_FILE = /\.(.*)$/;

export function middleware(request: NextRequest, response: NextResponse) {
  const shouldHandleLocale =
    !PUBLIC_FILE.test(request.nextUrl.pathname) &&
    !request.nextUrl.pathname.includes('/api/') &&
    request.nextUrl.locale === 'default';
  const redirectLocale = request.cookies.NEXT_LOCALE || 'en';

  return shouldHandleLocale
    ? NextResponse.redirect(`/${redirectLocale}${request.nextUrl.pathname.replace('/default', '')}`)
    : undefined;
}
