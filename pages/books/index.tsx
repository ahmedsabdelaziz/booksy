import {useEffect, useState} from 'react';
import type {NextPage, GetServerSideProps} from 'next';
import Link from 'next/link';
import {useTranslations} from 'next-intl';
import type {Book, GetBooksResponse} from '../../types/book';

export const getServerSideProps: GetServerSideProps<Props> = async ({query: {search}}) => {
  const query = new URLSearchParams();
  if (search) {
    query.set('search', search as string);
  }
  const response = await fetch(`http://gutendex.com/books?${query.toString()}`);
  const result = (await response.json()) as GetBooksResponse;

  return {
    props: {
      books: result.results,
      next: result.next,
    },
  };
};

interface Props {
  books: Book[];
  next: string | null;
}
const Books: NextPage<Props> = ({books, next}) => {
  const t = useTranslations();

  const [loadedBooks, setLoadedBooks] = useState(books);
  const [nextPageLink, setNextPageLink] = useState(next);

  useEffect(() => {
    async function onScrollToBottom() {
      if (!nextPageLink) {
        return;
      }

      const el = document.scrollingElement as HTMLElement;
      if (el.offsetHeight + el.scrollTop >= el.scrollHeight) {
        const response = await fetch(nextPageLink);
        const result = (await response.json()) as GetBooksResponse;

        setNextPageLink(result.next);
        setLoadedBooks(loadedBooks.concat(result.results));
      }
    }
    window.addEventListener('scroll', onScrollToBottom);
    onScrollToBottom();

    return () => window.removeEventListener('scroll', onScrollToBottom);
  }, [loadedBooks, nextPageLink]);

  return (
    <>
      <div className="grow grid gap-4 grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-8 overflow-y-auto h-full">
        {loadedBooks.map((book) => (
          <Link key={book.id} href={`/books/${book.id}`}>
            <a className="flex flex-col items-center gap-2 pb-2">
              <img alt={book.title} src={book.formats['image/jpeg']} className="h-36 mb-4" />
              <div className="w-full px-2 text-center font-semibold text-black/75 text-xl overflow-x-hidden whitespace-nowrap text-ellipsis">
                {book.title}
              </div>
              <div className="w-full px-2 text-center font-semibold text-gray-400/75 text-md overflow-x-hidden whitespace-nowrap text-ellipsis">
                {book.authors.map(({name}) => name).join(', ')}
              </div>
            </a>
          </Link>
        ))}
      </div>
      <div className="flex justify-center">
        {nextPageLink ? (
          `${t('loading')}...`
        ) : (
          <span className="py-12 text-2xl text-primary">{t('no-more-books')}</span>
        )}
      </div>
    </>
  );
};

export default Books;
