import type {NextPage, GetServerSideProps} from 'next';
import {useTranslations} from 'next-intl';
import Link from 'next/link';
import type {Book} from '../../types/book';

const links = [
  {mimeType: 'application/x-mobipocket-ebook', label: 'Kindle'},
  {mimeType: 'text/html', label: 'HTML'},
  {mimeType: 'text/plain; charset=utf-8', label: 'TXT'},
  {mimeType: 'application/zip', label: 'ZIP'},
  {mimeType: 'application/rdf+xml', label: 'RDF'},
  {mimeType: 'application/epub+zip', label: 'EBUP'},
];

export const getServerSideProps: GetServerSideProps<Props> = async ({query: {id}}) => {
  const response = await fetch(`http://gutendex.com/books/${id as string}`);
  const result = (await response.json()) as Book;

  return {
    props: {
      book: result,
    },
  };
};

interface Props {
  book: Book;
}
const BookDetails: NextPage<Props> = ({book}) => {
  const t = useTranslations();

  return (
    <>
      <div className="grid grid-cols-1 lg:grid-cols-2 bg-accent p-4 rounded-xl">
        <div>
          <h2 className="font-semibold text-3xl text-primary mb-4">{book.title}</h2>
          <div className="mb-6">
            <h3 className="font-semibold text-xl mb-2">{t('authors')}: </h3>
            <p className="text-lg text-gray-600">{book.authors.map(({name}) => name).join(', ')}</p>
          </div>
          <div className="mb-6">
            <h3 className="font-semibold text-xl mb-2">{t('bookshelves')}: </h3>
            <p className="text-lg text-gray-600">{book.bookshelves.join(', ')}</p>
          </div>
          <div className="mb-6">
            <h3 className="font-semibold text-xl mb-2">{t('links')}: </h3>
            <ul className="px-4 list-disc">
              {links.map(
                ({mimeType, label}) =>
                  book.formats[mimeType] && (
                    <li key={mimeType}>
                      <Link href={book.formats[mimeType]}>
                        <a
                          className="
                            relative
                            ring-0
                            text-gray-700 hover:text-primary focus:text-primary
                            transition-colors duration-100
                            after:content-['']
                            after:absolute
                            after:left-0
                            after:right-0
                            after:bottom-0
                            after:h-px
                            after:bg-primary
                            after:transition-transform
                            after:duration-300
                            after:origin-left
                            rtl:after:origin-right
                            after:scale-x-0
                            hover:after:scale-x-100
                            focus:after:scale-x-100
                            "
                        >
                          {label}
                        </a>
                      </Link>
                    </li>
                  ),
              )}
            </ul>
          </div>
        </div>
        <div className="flex justify-center p-4">
          <img
            alt={book.title}
            src={book.formats['image/jpeg']}
            className="max-w-full max-h-full"
          />
        </div>
      </div>
    </>
  );
};

export default BookDetails;
