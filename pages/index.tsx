import type {NextPage, GetServerSideProps} from 'next';
import {useTranslations} from 'next-intl';
import Image from 'next/image';
import Link from 'next/link';
import type {Book, GetBooksResponse} from '../types/book';
import illustration from '../public/book-illustration.svg';

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  // There's no way to sort the data, so we'll emulate it by get the first
  // 6 books in the query
  const response = await fetch('http://gutendex.com/books');
  const result = (await response.json()) as GetBooksResponse;

  return {
    props: {
      popularBooks: result.results.slice(0, 6),
    },
  };
};

interface Props {
  popularBooks: Book[];
}
const Home: NextPage<Props> = ({popularBooks}) => {
  const t = useTranslations();

  return (
    <>
      <div className="flex bg-accent rounded-xl">
        <div className="w-full md:w-1/2 px-12 flex flex-col justify-center gap-6">
          <h2 className="text-primary font-semibold text-6xl">{t('build-your-library')}</h2>
          <div className="w-7/12 text-2xl font-semibold text-gray-500/50">
            {t('buy-two-selected-books-and-get-one-for-free')}
          </div>
          <Link href="/books">
            <a className="w-fit py-1.5 px-5 rounded-md text-lg font-semibold bg-accent-dark text-white/90">
              {t('view-all')}
            </a>
          </Link>
        </div>
        <div className="w-0 md:w-1/2 flex justify-end">
          <Image src={illustration} alt="reading illustration" height={320} />
        </div>
      </div>
      <div className="grow flex flex-col">
        <div className="flex justify-between py-10">
          <h3 className="font-semibold text-3xl text-black/75">{t('popular-now')}</h3>
          <Link href="/books">
            <a className="font-semibold text-xl text-gray-400/75">{t('view-all')}</a>
          </Link>
        </div>
        <div className="grow grid gap-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6">
          {popularBooks.map((book) => (
            <Link key={book.id} href={`/books/${book.id}`}>
              <a className="flex flex-col items-center gap-2 pb-2">
                <img alt={book.title} src={book.formats['image/jpeg']} className="h-36 mb-4" />
                <div className="w-full px-2 text-center font-semibold text-black/75 text-xl overflow-x-hidden whitespace-nowrap text-ellipsis">
                  {book.title}
                </div>
                <div className="w-full px-2 text-center font-semibold text-gray-400/75 text-md overflow-x-hidden whitespace-nowrap text-ellipsis">
                  {book.authors.map(({name}) => name).join(', ')}
                </div>
              </a>
            </Link>
          ))}
        </div>
      </div>
    </>
  );
};

export default Home;
