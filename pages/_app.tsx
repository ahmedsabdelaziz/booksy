import type {AppProps} from 'next/app';
import {NextIntlProvider} from 'next-intl';
import en from '../i18n/en.json';
import ar from '../i18n/ar.json';
import {AnimatePresence, domAnimation, LazyMotion, m} from 'framer-motion';
import Header from '../components/Header';
import '../styles/globals.css';

const transition = {
  variants: {
    initial: {
      opacity: 0,
    },
    animate: {
      opacity: 1,
    },
    exit: {
      opacity: 0,
    },
  },
  transition: {
    duration: 0.3,
  },
};

const messages = {en, ar};

function MyApp({Component, pageProps, router}: AppProps) {
  const locale = router.locale == 'default' ? 'en' : (router.locale as keyof typeof messages);

  return (
    <div dir={messages[locale]?.direction} className="min-h-screen flex flex-col">
      <NextIntlProvider messages={messages[locale]}>
        <Header />
        <main className="grow flex justify-center px-4">
          <div className="container">
            <LazyMotion features={domAnimation}>
              <AnimatePresence exitBeforeEnter>
                <m.div
                  key={router.route}
                  style={{flex: '1 1 0%'}}
                  initial={router.isSsr === false && 'initial'}
                  animate="animate"
                  exit="exit"
                  variants={transition.variants}
                  transition={transition.transition}
                >
                  <Component {...pageProps} />
                </m.div>
              </AnimatePresence>
            </LazyMotion>
          </div>
        </main>
      </NextIntlProvider>
    </div>
  );
}

export default MyApp;
