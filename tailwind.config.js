module.exports = {
  mode: 'jit',
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: '#433D8A',
        },
        accent: {
          DEFAULT: '#F3E7DD',
          dark: '#C2A792',
        },
      },
    },
  },
  variants: {},
  plugins: [],
};
