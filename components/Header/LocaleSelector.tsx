import Link from 'next/link';
import {useRouter} from 'next/router';
import {useTranslations} from 'next-intl';
import {Menu, Transition} from '@headlessui/react';

const localeNames = {
  en: 'English',
  ar: 'العربية',
};

export default function LocaleSelector() {
  const {asPath, locales, locale: currentLocale} = useRouter();
  const t = useTranslations();

  return (
    <Menu
      as="div"
      className="
        relative
        origin-bottom-right rtl:origin-bottom-left
        inline-block
        text-left rtl:text-right
        "
    >
      <div>
        <Menu.Button
          className="
            inline-flex justify-center
            w-full
            text-lg leading-tight font-semibold
            text-gray-500 hover:text-gray-700
            "
        >
          {t('locale_short')}
        </Menu.Button>
      </div>
      <Transition
        as={Menu.Items}
        appear
        enter="transition-all ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
        className="
        absolute
        right-0 rtl:right-full
        rtl:translate-x-full
        origin-top-right rtl:origin-top-left
        w-28 mt-2 py-1
        rounded-md
        shadow-lg
        bg-white
        ring-1 ring-black ring-opacity-5
        focus:outline-none
        overflow-hidden
        "
      >
        {/* Remove the 'default' locale */}
        {locales!.slice(1).map((locale) => (
          <Menu.Item key={locale}>
            <Link href={asPath} locale={locale}>
              <a
                className={`
                    ${
                      locale == currentLocale
                        ? 'bg-primary text-white hover:text-gray-200'
                        : 'text-gray-900 hover:text-gray-600'
                    }
                    flex
                    items-center
                    w-full px-2 py-2
                    `}
              >
                {localeNames[locale as keyof typeof localeNames]}
              </a>
            </Link>
          </Menu.Item>
        ))}
      </Transition>
    </Menu>
  );
}
