import Link from 'next/link';
import {useRouter} from 'next/router';
import {useTranslations} from 'next-intl';

const links = [
  {
    href: '/',
    labelKey: 'home',
  },
  {
    href: '/books',
    labelKey: 'all-books',
  },
];

export default function Nav() {
  const t = useTranslations();
  const {pathname} = useRouter();

  return (
    <nav>
      <ul className="flex justify-center gap-8 py-4 text-lg font-semibold">
        {links.map(({href, labelKey}) => (
          <li key={href}>
            <Link href={href}>
              <a
                className={`
                  relative
                  after:content-['']
                  after:absolute after:top-full after:left-1/2 after:-translate-x-1/2
                  after:w-2 after:h-2 after:rounded-full
                  after:transition-colors after:duration-300
                  ${
                    href == pathname
                      ? `text-primary delay-300
                         after:bg-primary after:delay-300
                         `
                      : `text-gray-400 hover:text-gray-600
                         after:bg-transparent
                         `
                  }
                  transition-colors duration-300
                  `}
              >
                {t(labelKey)}
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
}
