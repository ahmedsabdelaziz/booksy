import {useState, useEffect, useMemo} from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';
import {useTranslations} from 'next-intl';
import {Transition} from '@headlessui/react';
import {SearchIcon} from '@heroicons/react/outline';
import type {Book, GetBooksResponse} from '../../types/book';
import debounce from '../../lib/debounce';
import clearFocus from '../../lib/clearFocus';

export default function Search() {
  const router = useRouter();
  const t = useTranslations();

  const [isFocused, originalSetIsFocused] = useState(false);
  // Moving from a link item to another will trigger the blur event, then the focus event.
  // So if user tabs qquickly between elements, the dropdown will flicker.
  // Debouncing the state setter will fix this issue.
  const setIsFocused = useMemo(() => debounce(originalSetIsFocused, 100), [originalSetIsFocused]);

  const [search, setSearch] = useState('');
  const [result, setResult] = useState<Book[]>([]);
  useEffect(() => {
    if (!search) {
      setResult([]);
      return;
    }

    const controller = new AbortController();
    const {signal} = controller;
    const query = new URLSearchParams({search: encodeURIComponent(search), count: '3'});
    fetch(`http://gutendex.com/books/?${query.toString()}`, {
      signal,
      headers: {
        Accept: 'application/json',
      },
    })
      .then((response) => response.json())
      .then(({results}: GetBooksResponse) => setResult(results.slice(0, 5)))
      .catch((error) => {
        if (error.name == 'AbortError') {
          return;
        }
      });

    return () => controller.abort();
  }, [search]);

  return (
    <div
      className="relative w-0 md:w-1/2 overflow-hidden md:overflow-visible"
      onFocus={() => setIsFocused(true)}
      onBlur={() => setIsFocused(false)}
      onKeyUp={(event) => {
        if (event.key == 'Escape') {
          clearFocus();
          return;
        }

        if (event.key == 'Enter' && (event.target as HTMLElement).tagName == 'A') {
          clearFocus();
          return;
        }
      }}
      onClick={(event) => {
        if ((event.target as HTMLElement).tagName == 'A') {
          clearFocus();
          return;
        }
      }}
    >
      <form
        className="
          w-full h-full
          py-2 px-12
          rounded-md
          flex
          bg-gray-100
          text-lg
          focus-within:ring-2 focus-within:ring-primary focus-within:ring-opacity-75
          "
        onSubmit={(event) => {
          event.preventDefault();
          let query = '';
          if (search) {
            query = new URLSearchParams({search}).toString();
          }

          router.push(`/books?${query}`);
          clearFocus();

          return false;
        }}
      >
        <input
          className="
            grow
            bg-transparent
            text-gray-500 font-semibold
            placeholder:text-gray-400
            focus-visible:ring-0
            "
          placeholder={t('search-placeholder')}
          value={search}
          onInput={(event) => setSearch(event.currentTarget.value)}
        />
        <button type="submit" tabIndex={-1}>
          <SearchIcon className="w-5 text-gray-700" />
        </button>
      </form>
      <Transition
        as="ul"
        show={isFocused && result.length > 0}
        enter="transition-all ease-out duration-100"
        enterFrom="transform scale-y-0"
        enterTo="transform scale-y-100"
        leave="transition ease-in duration-100"
        leaveFrom="transform scale-y-100"
        leaveTo="transform scale-y-0"
        className="
          absolute
          left-0 right-0
          mt-2
          bg-white
          rounded-md
          shadow-lg
          origin-top
          z-10
          "
      >
        {result.map((book) => (
          <li key={book.id} className="flex py-2 px-4 ">
            <Link href={`/books/${book.id}`}>
              <a
                className={`
                  relative
                  inline-block max-w-full px-1
                  ring-0
                  hover:text-primary focus:text-primary
                  overflow-x-hidden whitespace-nowrap text-ellipsis
                  transition-colors
                  duration-100
                  focus:outline-none
                  after:content-['']
                  after:absolute
                  after:left-0
                  after:right-0
                  after:bottom-0
                  after:h-px
                  after:bg-primary
                  after:transition-transform
                  after:duration-300
                  after:origin-left
                  rtl:after:origin-right
                  after:scale-x-0
                  hover:after:scale-x-100
                  focus:after:scale-x-100
                  `}
              >
                {book.title}
              </a>
            </Link>
          </li>
        ))}
      </Transition>
    </div>
  );
}
