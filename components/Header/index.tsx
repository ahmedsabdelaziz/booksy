import Link from 'next/link';
import Search from './Search';
import LocaleSelector from './LocaleSelector';
import Nav from './Nav';

export default function Header() {
  return (
    <header className="flex justify-center py-6 px-4">
      <div className="container">
        <div className="flex py-4 items-center">
          <h1 className="grow font-bold text-2xl">
            <Link href="/">
              <a>Booksy</a>
            </Link>
          </h1>
          <Search />
          <div className="w-1/4 flex justify-end items-end gap-8">
            <LocaleSelector />
          </div>
        </div>
        <Nav />
      </div>
    </header>
  );
}
